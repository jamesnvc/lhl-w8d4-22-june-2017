//
//  TodayViewController.swift
//  NmuberToday
//
//  Created by James Cash on 22-06-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

import UIKit
import NotificationCenter
import NumberShared

class TodayViewController: UIViewController, NCWidgetProviding {
        
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var numberStepper: UIStepper!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        if let val = UserDefaults(suiteName: groupName)?.float(forKey: defaultsKey) {
            numberLabel.text = "\(val)"
            numberStepper.value = Double(val)
        }
    }

    @IBAction func numberChanged(_ sender: UIStepper) {
        let val = Float(sender.value)
        numberLabel.text = "\(val)"
        UserDefaults(suiteName: groupName)?.set(val, forKey: defaultsKey)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        completionHandler(NCUpdateResult.newData)
    }
    
}
