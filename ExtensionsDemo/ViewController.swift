//
//  ViewController.swift
//  ExtensionsDemo
//
//  Created by James Cash on 22-06-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

import UIKit
import NumberShared

class ViewController: UIViewController {

    @IBOutlet weak var numberSlider: UISlider!
    @IBOutlet weak var numberLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        restoreValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func numberChanged(_ sender: UISlider) {
        numberLabel.text = "\(sender.value)"
        saveValue(sender.value)
    }

    func saveValue(_ val: Float) {
        UserDefaults(suiteName: groupName)?.set(val, forKey: defaultsKey)
    }

    func restoreValue() {
        guard let val = UserDefaults(suiteName: groupName)?.float(forKey: defaultsKey) else { return }

        numberSlider.value = val
        numberLabel.text = "\(val)"
    }
}

