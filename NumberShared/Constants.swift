//
//  Constants.swift
//  ExtensionsDemo
//
//  Created by James Cash on 22-06-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

import Foundation

public let groupName = "group.occasionallycogent.testing"
public let defaultsKey = "savedNumber"
